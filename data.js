var data = {
  "misc": {
    "info" : {
      "name" : "Jakob Hostnik",
      "username" : "hostops",
      "image" : "profile.png",
      "born" : "1998-06-20",
      "headline" : "Bachelor’s Degree in Computer Science and Mathematics (University studies)",
      "job" : "DevOps / DevSecOps engineer, technical team leader",
      "address" : "Ljubljana, Slovenia",
      "email" : "jakob@hostnik.si",
      "phone" : "+386 51 232 527",
      "personalityTraits" : ["integrity", "honesty", "responsibility", "compassion", "reliablility", "encouragingness", "thoroughness"],
      "interests" : ["devops", "family", "programming", "home automation", "electronics"],
      "languages" : {
        "Slovene" : "native",
        "English" : "full professional",
        "Croatian" : "elementary"
      },
    }
  },
  "timeline" : [ // cards have start and end
    {
      "headline" : "Project Ne",
      "description" : "A library that helps you create arcade games in a simple way using few lines. The purpose of project Ne is to make children interested in coding.",
      "tags" : ["open source", "software", "side project"],
      "start" : "2018",
      "end" : "2018",
      "links" : ["https://github.com/hostops/Ne"],
      "media" : [{ "full" : "project-ne/code-example.jpg", "thumb" : "project-ne/code-example_thumb.jpg"}, { "full" : "project-ne/screenshot.jpg", "thumb" : "project-ne/screenshot_thumb.jpg"}],
      "skills" : ["JavaScript", "HTML", "CSS"]
    },
    {
      "headline" : "The labyrinth",
      "description" : "My school mate and I created a small wooden labyrinth. You put a small ball in it and controll the labyrinth's slope by tilting your hand. We also wrote a scientific article about this project for the magazine Svet Elektronike.",
      "tags" : ["publication", "side project"],
      "start" : "2016",
      "links" : ["https://svet-el.si/revija/pretekle-stevilke/2016-242-1/"],
      "media" : [{ "full" : "labyrinth/photo1.jpg", "thumb" : "labyrinth/photo1_thumb.jpg"}, { "full" : "labyrinth/photo2.jpg", "thumb" : "labyrinth/photo2_thumb.jpg"}],
      "skills" : ["Arduino", "C++", "MPU6050", "servo", "circuit design"]
    },
    {
      "headline" : "SloCode forum",
      "description" : "I built a forum from scratch using php for learning programming and web technologies.",
      "tags" : ["open source", "side project"],
      "start" : "2015",
      "end" : "2016",
      "links" : ["https://github.com/hostops/slocode"],
      "media" : [{ "full" : "slocode/image1.JPG", "thumb" : "slocode/image1_thumb.jpg"}, { "full" : "slocode/image2.png", "thumb" : "slocode/image2_thumb.png"}],
      "skills" : ["PHP", "MySQL", "CSS", "HTML", "JavaScript"]
    },
    {
      "headline" : "PlusPage",
      "description" : `
      In the second year of high school I created PlusPage - a personal dashboard page, which was similar to dead iGoogle.
      The whole experience was based on iframes - websites you placed in your website.
      There was a simple authetication and each user had his own dashboard he could save in database.
      Fun fact - I did not use GET or POST requests to transfer data to server.
      I used cookies since it was the only technology I knew and was already presented in a demo project in the template of Netbeans.
      There was a lot of bad code, but I finished the project and I deployed it in openshift and provided a free domain for it.
      `,
      "tags" : ["open source", "side project"],
      "start" : "2014",
      "end" : "2015",
      "links" : ["https://github.com/hostops/pluspage"],
      "media" : [{ "full" : "pluspage/photo1.jpeg", "thumb" : "pluspage/photo1_thumb.jpeg"}, { "full" : "pluspage/photo2.jpg", "thumb" : "pluspage/photo2_thumb.jpg"}, { "full" : "pluspage/photo3.png", "thumb" : "pluspage/photo3_thumb.png"}],
      "skills" : ["Java", "MySQL", "CSS", "HTML", "JavaScript"]
    },
    {
      "headline" : "Web pages creation",
      "description" : `
      At the beginning of high school I created a few web pages - some in wordpress and some in other technologies.
      Some are better some are worse but this is how I got into web development.
      `,
      "tags" : ["side project"],
      "start" : "2013",
      "end" : "2015",
      "links" : ["http://nizi.si/", "http://www.zupnija-smartno-pri-litiji.rkc.si/", "https://musips.com", "http://www.smartno.sls.si/"],
      "media" : [],
      "skills" : ["Java", "MySQL", "CSS", "HTML", "JavaScript", "PHP", "WordPress", "Google blogs"]
    },
    {
      "headline" : "Full stack software developer",
      "description" : `
      I worked in a team, developed new features and fixed bugs in a web platform. 
      I mostly continued work of previous developers.
      I was also responsible for testing out new technologies that we could use to improve our process.
      `,
      "tags" : ["work experience", "role", "company:HPE"],
      "start" : "2017-06-19",
      "end" : "2019-06-04",
      "links" : [],
      "media" : [],
      "projects" : {
        "CALMS backend" : {"skills" : ["Java", "Playframework", "Spring", "Hibernate", "Siena" , "CassandraDB", "KairosDB", "PostgreSQL", "JHipster", "TimeSeries data", "BigData"]},
        "CALMS frontend" : {"skills" : ["HTML", "jQuery", "AngularJS", "Angular", "CSS(LESS)"]},
        "IOT devices" : {"skills" : ["Beckhoff PLC", "Structured text", "Arduino", "C#", "Windows CE", "3D printing", "circuit design"]},
        "Server management / Cloud" : { "skills" :["AWS (E2C, IOT, SNS, IAM, ...)", "Linux server", "Apache", "SystemD"]},
      }
    },
    {
      "headline" : "DevOps / DevSecOps engineer, CTO (technical team leader)",
      "description" : `
      At first we were maintaining code on badly designed projects and frameworks that stopped existing.
      At some point maintaining required more resources than development so we had to redesign our whole system but wanted to reuse as much code as possible.
      At the time we refreshed our team and I was promoted to CTO role.
      We started redesigning the project and started using proper microservices technology, which proved itself to be a good choice.
      The best way to manage and scale microservices is by using Kubernetes. 
      Since I was still in college and DevOps is my passion, I took classes thet covered Kubernetes, Docker and DevOps.
      We moved to Kubernetes and we are successfully managing it in production using proper monitoring, logging and CI/CD.

      Because of my ideas and suggestions, we started a few very successful projects and improved existing ones.
      **After some time our director recognised me as one of most valuable employees (MVP) and offered me a small amount of percentages of the company.**
      `,
      "tags" : ["work experience", "role", "company:HPE"],
      "start" : "2019-06-04",
      "end" : new Date(),
      "links" : [],
      "media" : [],
      "skills" : ["leadership"],
      "projects" : {
        "Backends" : {"skills" : ["Java", "Spring" , "CassandraDB", "KairosDB", "PostgreSQL", "JHipster", "TimeSeries data", "BigData", "Rust", "TimeScaleDB", "MQTT", "HTTP", "OpenAPI", "Redis", "JWT authentication"]},
        "Frontends" : {"skills" : ["Angular", "HTML", "CSS", "NodeJS"]},
        "IOT devices" : {"skills" : ["Linux", "Java", "Embedded PI", "Raspberry PI", "MQTT", "MODBUS", "RS485", "RS232", "ADS", "OPC UA", "HTTP", "AsyncAPI"]},
        "Server management / Cloud" : { "skills" :["AWS (E2C, IOT, SNS, IAM, ...)", "Linux server", "Apache", "SystemD"]},
        "DevOps / DevSecOps" : { "skills" :["Kubernetes", "KOPS", "GitOps", "Argo CD", "BitBucket pipelines", "Monitoring/Logging (Grafana, Prometheus, Loki, ...)", "Backup management (Velero)", "Security and certificate management (CertManager, Step certificates)", "MQTT", "WordPress", "Weblate", "Redis", "TimeScaleDB", "Postgres", "High availability", "Zerro down time deployments", "AWS (E2C, IOT, SNS, IAM, ...)"]},
      }
    },
    {
      "headline" : "Primary school",
      "institution" : "OŠ Šmartno, Šmartno pri Litiji",
      "educationLevelNumber" : "2",
      "programName" : "Primary school",
      "description" : "Local primary school.",
      "place" : "Šmartno pri Litiji, Slovenia",
      "tags" : ["education"],
      "start" : "2004",
      "end" : "2013",
      "links" : ["https://os-smartnolitija.si"],
      "media" : [{ "full" : "os-smartno.jpg", "thumb" : "os-smartno_thumb.jpg"}],
      "linkedCards" : [
        "primary school"
      ]
    },
    {
      "start" : "2010",
      "end" : "2013",
      "photos" : [],
      "links" : [],
      "headline" : "Computer science classes",
      "description" : "Last three years of primary school I took optional computer science classes.",
      "tags" : ["primary school"],
      "skills" : ["HTML 4", "Microsoft Word", "Microsoft Excel", "Microsoft Power Point"]
    },
    {
      "headline" : "Technical grammar school graduate",
      "institution" : "Vegova Ljubljana",
      "educationLevelNumber" : "5",
      "programName" : "Technical grammar school diploma",
      "earnedTitle" : "Technical grammar school graduate",
      "place" : "Ljubljana, Slovenia",
      "description" : "Technical grammar school in Ljubljana, where I enrolled into computer science module.",
      "tags" : ["education"],
      "start" : "2013",
      "end" : "2017",
      "links" : ["https://www.vegova.si"],
      "media" : [{ "full" : "vegova.jpg", "thumb" : "vegova_thumb.jpg"}],
      "skills" : ["Java", "Java Servlets", "HTML", "JavaScript", "CSS", "SQL", "networking", "Arduino", "C++", "Computer Science"],
      "linkedCards" : [
        "high school"
      ]
    },
    {
      "start" : "2015",
      "photos" : [],
      "links" : [],
      "headline" : "Linux club",
      "description" : "I was a member of a Linux course and club mentored by Alexei Drake",
      "tags" : ["high school", "education"],
      "skills" : ["Linux desktop", "Linux server"]
    },
    {
      "start" : "2015",
      "end" : "2016",
      "photos" : [],
      "links" : ["https://plus.si.cobiss.net/opac7/bib/15385651", "contents/online-advertising/online-advertising.pdf"],
      "headline" : "Research paper about online advertising",
      "description" : "The paper was by my school mate and I and was ranked on a national level.",
      "tags" : ["publication", "high school", "education"],
      "skills" : ["online advertising", "Google Ads"]
    },
    {
      "start" : "2016",
      "photos" : ["coffe-maker/photo1.jpg", "coffe-maker/photo2.jpg"],
      "links" : ["contents/coffe-maker/coffe-maker.pdf", "https://github.com/hostops/CoffeMachine"],
      "headline" : "Coffee maker",
      "description" : "As a project in my third year of high school I created a coffe maker and a seminar paper about it.",
      "tags" : ["seminar paper", "open source", "high school", "education"],
      "skills" : ["Arduino", "C++", "Servo", "LCD display", "3D printing", "CNC milling", "circuit design"]
    },
    {
      "start" : "2016",
      "photos" : ["snake-game/photo1.png", "snake-game/photo2.png", "snake-game/photo3.png"],
      "links" : ["https://hostops.github.io/Snake/", "https://github.com/hostops/Snake"],
      "headline" : "Simple snake game framework",
      "description" : "I went above and beyond when doing homework and created a framework instead of a simple snake game.",
      "tags" : ["open source", "high school", "education"],
      "skills" : ["Java", "JavaFX"]
    },
    {
      "start" : "2016",
      "photos" : ["smart-home/image1.png", "smart-home/image2.png", "smart-home/image3.jpg", "smart-home/image4.png"],
      "links" : ["contents/smart-home/smart-home.pdf", "https://github.com/hostops/SmartWidgets"],
      "headline" : "Smart house implementation",
      "description" : "For final matura project I implemented smarthome using OpenHab and MQTT protocol. I also integrated voice control in Cortana and Windows 10.",
      "tags" : ["seminar paper", "open source", "high school", "education"],
      "skills" : ["DevOps", "MQTT", "Linux server", "Cortana voice control", "ESP8266", "Arduino", "C++", "Raspberry PI", "3D printing", "CNC milling", "circuit design"]
    },
    {
      "start" : "2017",
      "photos" : [],
      "links" : [],
      "headline" : "Final matura exam results",
      "description" : "I got the highest possible mark in mathematics (mark 8) and computes science (mark 5).",
      "tags" : ["high school", "education"],
      "skills" : ["Java", "HTML", "CSS", "Team work"]
    },
    {
      "headline" : "Faculty of Computer Science and Mathematics (UNI)",
      "institution" : "University of Ljubljana - Faculty of Computer and Information Science",
      "educationLevelNumber" : "7",
      "programName" : "Computer Science and Mathematics (UNI)",
      "earnedTitle" : "Bachelor’s Degree in Computer Science and Mathematics (University studies)",
      "description" : `Interdisciplinary university study programme. Half of the classes is in mathematics and the other half in computer science.`,
      "place" : "Ljubljana, Slovenia",
      "tags" : ["education"],
      "start" : "2017",
      "end" : "2021",
      "links" : ["https://fri.uni-lj.si/en", "https://fri.uni-lj.si/en/study-programme/computer-science-and-mathematics-1st-cycle", "contents/electronic-transcriptions.pdf"],
      "media" : [{ "full" : "fri.JPG", "thumb" : "fri_thumb.jpg"}],
      "subjectsList" : ["Algorithms and data structures 1", "Algorithms and data structures 2", "Analysis 1", "Analysis 2", "Analysis 3", "Basics of Databases", "Coding Theory and Cryptography", "Combinatorics", "Computability and Computational Complexity", "Computer Communications", "Computer Science Skills", "Computer Systems Architecture", "Discrete Structures 1", "Discrete Structures 2", "Introduction to Artificial Intelligence", "Introduction to Digital Circuits", "Linear algebra", "Numerical methods", "Optimization Methods", "Platform Based Development", "Principles of Programming Languages", "Probability and Statistics", "Programming 1", "Programming 2", "Software Development Processes", "Sports COD", "Topics in Computer and Information Science", "Topics in Mathematics", "Undergraduate Thesis", "Web Programming"],
      "skills" : ["computer science", "networking", "algorithms", "databases", "application development", "frontend", "backend", "algebra", "analysis", "combinatorics", "statistics", "machine learning"],
      "linkedCards" : [
        "faculty"
      ]
    },
    {
      "start" : "2019",
      "end" : "2020",
      "photos" : [],
      "links" : ["https://www.stanford.edu", "https://online.stanford.edu/courses/cs224w-machine-learning-graphs", "http://web.stanford.edu/class/cs224w/", "https://online.stanford.edu/programs/mining-massive-data-sets-graduate-certificate"],
      "headline" : "Class at Stanford university - Machine learning with graphs - CS224W",
      "description" : "I took a special class on Stanford university about machine learning on massive graphs. The class was lead by Jure Leskovec and his assistants. It was the hardest class I ever took and got a final grade 8 out of 10.",
      "tags" : ["faculty", "education"],
      "skills" : ["Python", "Machine learning", "Machine learning with graphs", "Mining Massive Data Sets Graduate Certificate"]
    },
    {
      "start" : "2019",
      "end" : "2020",
      "photos" : [],
      "links" : ["https://uporabna-informatika.si/index.php/ui/article/view/85/82", "https://plus.si.cobiss.net/opac7/bib/27335683", "https://uporabna-informatika.si/index.php/ui/article/view/85", "https://uporabna-informatika.si/index.php/ui", "https://github.com/AnejSvete/cs224w-project"],
      "headline" : "Research: It is not just about the melody",
      "description" : "As a project in class Machine learning with graphs at Stanford university my school mate and I researched bias in Eurovision song contest. Our research paper got a mark 10 and we also published an article in the scientific magazine Applied Informatics Journal (Uporabna informatika).",
      "tags" : ["publication", "open source", "faculty", "education"],
      "skills" : ["Python", "Machine learning", "Machine learning with graphs", "Mining Massive Data Sets Graduate Certificate"]
    },
    {
      "start" : "2019",
      "end" : "2020",
      "photos" : [],
      "links" : ["https://fri.uni-lj.si/en/course/63767"],
      "headline" : "Class about Containers, Kubernetes and DevOps",
      "description" : "I took a practical class about containers, devops and kubernetes. The class was lead by Matjaž Pančur.",
      "grade" : "10",
      "tags" : ["faculty", "education"],
      "skills" : ["Kubernetes", "DevOps", "Docker", "containers", "DevSecOps", "Virtualization", "load balancing", "scalability", "high availability", "CI/CD", "CNCF", "best practices"]
    },
    {
      "start" : "2019",
      "end" : "2020",
      "photos" : [],
      "links" : ["https://github.com/hostops/generator-jhipster-javaee", "https://www.jhipster.tech/", "https://npmjs.org/package/generator-jhipster-javaee", "https://www.jhipster.tech/modules/marketplace/#/details/generator-jhipster-javaee"],
      "headline" : "JavaEE jhipster generator",
      "description" : `
      Since I already had some experiences in software development, the professor of the class "Software Development Processes" agreed I could help him and his team in a laboratory.
      I created a generator for their implementation of JavaEE as a JHipster module.
      `,
      "tags" : ["faculty", "education"],
      "skills" : ["Code generation", "Java", "JavaEE", "JHipster", "KumuluzEE"]
    },
    {
      "start" : "2021-02-24",
      "photos" : ["diploma/image1.png", "diploma/image2.jpg", "diploma/image3.png", "diploma/image4.png", "diploma/image5.png", "diploma/image6.png", "diploma/image7.png", "diploma/image8.png", "diploma/image9.png"],
      "links" : ["https://repozitorij.uni-lj.si/IzpisGradiva.php?id=124860&lang=slv"], //, "link do uradne verzije", "link do cobissa"
      "headline" : "Diploma thesis: Connecting Kubernetes clustsers",
      "description" : `For my final work in facullty I wrote a diploma thesis about connecting multiple Kubernetes clusters.
      I reviewed different approaches and implemented solutions for real world problems such as minimising geo-location latency, edge clusters and isolation of applications using multiple clusters.`,
      "tags" : ["publication", "open source", "faculty", "education"],
      "mentor" :  "izr. prof. dr. Mojca Ciglarič",
      "comentor" : "asist. dr. Matjaž Pančur",
      "grade" : "10",
      "skills" : ["K3S", "Edge clusters", "Kubernetes", "KubeFed", "Cilium", "GitOps", "ArgoCD", "DevOps"]
    },
    {
      "headline" : "Looking for responsibility in the cyber age",
      "description" : "Meetup about ethics of development after Cambridge Analytics affair. ",
      "guests" : [
        {"guest" : "Katja Koren Ošljak", "from" : "CodeWeek.eu"},
        {"guest" : "Žiga Stopinšek", "from" : "Zemanta"}, 
        {"guest" : "Matjaž Drev", "from" : "Office of the Information Commissioner"},
        {"guest" : "Domen Savič", "from" : "Državljan D"}
      ],
      "tags" : ["meetup"],
      "start" : "2018-05-01",
      "links" : ["https://www.facebook.com/events/602052740148938/"],
      "media" : [{ "full" : "responsibility-cyber-age/image1.jpg", "thumb" : "responsibility-cyber-age/image1_thumb.jpg"}]
    },
    {
      "headline" : "Living on the bleeding edge of Big Data",
      "description" : "Your boss gave you some data. A lot of data. You have to process it. Fast. Your back of the envelope calculation says you need a month to do it. Your boss needs it in a week. What do you do? Throw more hardware at it, of course. But with great power, comes great responsibility. We'll go through the above process on a couple of examples and learn that we might need to care about malloc and memory management even if all of our code is written in Python, and that learning Computer Architecture is very useful if you're trying to grep through 50 terabytes of data. We might even learn that Linux behaves like a bad airline... sometimes.", // fix description
      "tags" : ["meetup"],
      "guests" : [{"guest" : "Andrej Krevl", "from" : "Stanford University"}],
      "start" : "2018-01-01",
      "links" : ["https://www.meetup.com/vblatu/events/245883961/", "https://bitbucket.org/akrevl/vblatu2018/src/master/"],
      "media" : [{ "full" : "bleeding-edge-big-data/image1.jpg", "thumb" : "bleeding-edge-big-data/image1_thumb.jpg"}, { "full" : "bleeding-edge-big-data/image2.jpg", "thumb" : "bleeding-edge-big-data/image2_thumb.jpg"}, { "full" : "bleeding-edge-big-data/image3.jpg", "thumb" : "bleeding-edge-big-data/image3_thumb.jpg"}, { "full" : "bleeding-edge-big-data/image4.jpg", "thumb" : "bleeding-edge-big-data/image4_thumb.jpg"}]
    },
    {
      "headline" : "Machine learning in the real world",
      "description" : "Pinterest is a visual discovery application powered by machine learning. At Pinterest we use machine learning to model relationships between pins, understand style and visual signal, handle cold-start problems, and provide personalized real-time recommendations to over 200M users all over the world. In this talk Jure will give an overview of the machine learning efforts and solutions at Pinterest. He will focus on systems and effective engineering choices that enable productive machine learning development and allow engineers to effectively develop, test, and deploy machine-learning-based products.",
      "tags" : ["meetup"],
      "guests" : [{"guest" : "Jure Leskovec", "from" : "Stanford University, Pinterest"}],
      "start" : "2017-12-01",
      "links" : ["https://www.meetup.com/vblatu/events/245754674/", "https://fri.uni-lj.si/sl/novice/novica/predavanje-vblatu-dr-jure-leskovec-machine-learning-real-world"],
      "media" : [{ "full" : "bleeding-edge-big-data/image1.jpg", "thumb" : "bleeding-edge-big-data/image1_thumb.jpg"}, { "full" : "bleeding-edge-big-data/image2.jpg", "thumb" : "bleeding-edge-big-data/image2_thumb.jpg"}, { "full" : "bleeding-edge-big-data/image3.jpg", "thumb" : "bleeding-edge-big-data/image3_thumb.jpg"}, { "full" : "bleeding-edge-big-data/image4.jpg", "thumb" : "bleeding-edge-big-data/image4_thumb.jpg"}]
    },
    {
      "headline" : "Java 9 release party",
      "description" : "Meetup about changes in Java 9",
      "tags" : ["meetup"],
      "guests" : [{"guest" : "Tomaž Cerar", "from" : "OpenJDK Quality Outreach"}],
      "start" : "2017-09-01",
      "links" : ["https://www.meetup.com/OpenBlend-Slovenian-Java-User-Group/events/243269060/"],
      "media" : []
    },
    {
      "headline" : "EESTEC Challange",
      "description" : `EESTech Challenge is an international student competition organized by the European Society of Electrical and Computer Engineering Students EESTEC.`,
      "tags" : ["hackathon"],
      "start" : "2019-04-06",
      "links" : ["https://ec.eestec-lj.org/", "https://github.com/hostops/estech-challenge"],
      "media" : [{ "full" : "es-tech/certificate.jpeg", "thumb" : "es-tech/certificate_thumb.jpeg"}, { "full" : "es-tech/image1.jpg", "thumb" : "es-tech/image1_thumb.jpg"}, { "full" : "es-tech/image2.png", "thumb" : "es-tech/image2_thumb.png"}, { "full" : "es-tech/image3.jpg", "thumb" : "es-tech/image3_thumb.jpg"}, { "full" : "es-tech/image4.jpg", "thumb" : "es-tech/image4_thumb.jpg"}, { "full" : "es-tech/image5.jpg", "thumb" : "es-tech/image5_thumb.jpg"}, { "full" : "es-tech/image6.jpg", "thumb" : "es-tech/image6_thumb.jpg"}, { "full" : "es-tech/image7.jpg", "thumb" : "es-tech/image7_thumb.jpg"}, { "full" : "es-tech/image8.jpg", "thumb" : "es-tech/image8_thumb.jpg"}, { "full" : "es-tech/image9.jpg", "thumb" : "es-tech/image9_thumb.jpg"}, { "full" : "es-tech/image10.jpg", "thumb" : "es-tech/image10_thumb.jpg"}, { "full" : "es-tech/image11.jpg", "thumb" : "es-tech/image11_thumb.jpg"}, { "full" : "es-tech/image12.jpg", "thumb" : "es-tech/image12_thumb.jpg"}],
      "team" : {
        "name" : "11100010",
        "members" : ["Jakob Hostnik", "Miha Lazar", "Fares Villca"]
      },
      "place" : "Faculty of Electrical Engineering in Ljubljana",
      "project" : {
        "name" : "Danffos challenge - smart controlling devices in water energy network",
        "description" : `In 24 hours we created:
        - A mobile app for network maintainer to add devices in water network
        - Smart IOT device for sending data from Danffos controller to web aplication
        - Web application where you could monitor data from sensors and add new devices.
          App supported JWT authentication, user management and languages.
          We setup everything on servers in a cloud.
        `
      },
      "accomplishments" : "First award in Danffos challenge",
      "skills" : ["Android", "QR", "Raspberry PI", "Linux", "Java", "REST", "Spring", "Angular", "OpenAPI", "Arduino", "JHipster"]
    },
    {
      "headline" : "Travel hackathon",
      "description" : `
      Travel hackathon was organised by Coding community behind Kiwi.com. 
      The only rule was it had to be connected with traveling.
      `,
      "tags" : ["hackathon"],
      "start" : "2019-05-10",
      "links" : ["https://github.com/hostops/whereto", "https://www.facebook.com/media/set/?vanity=codekiwicom&set=a.2347231945597227", "https://www.facebook.com/media/set/?vanity=codekiwicom&set=a.2347231945597227", "https://partners.kiwi.com/presenting-tequila-revolution-travel-industry/", "https://www.poligon.si/", "https://code.kiwi.com/", "https://hack.travel/", "https://www.facebook.com/codekiwicom/"],
      "media" : [{ "full" : "travel-hackathon/photo1.jpg", "thumb" : "travel-hackathon/photo1_thumb.jpg"}, { "full" : "travel-hackathon/photo2.jpg", "thumb" : "travel-hackathon/photo2_thumb.jpg"}, { "full" : "travel-hackathon/photo3.jpg", "thumb" : "travel-hackathon/photo3_thumb.jpg"}, { "full" : "travel-hackathon/photo4.jpg", "thumb" : "travel-hackathon/photo4_thumb.jpg"}, { "full" : "travel-hackathon/photo5.jpg", "thumb" : "travel-hackathon/photo5_thumb.jpg"}, { "full" : "travel-hackathon/photo6.jpg", "thumb" : "travel-hackathon/photo6_thumb.jpg"}],
      "team" : {
        "name" : "Samo in Frodo",
        "members" : ["Jakob Hostnik", "Miha Lazar", "Fares Villca"]
      },
      "place" : "Poligon Ljubljana",
      "project" : {
        "name" : "WhereTo - application that helps you and your people decide where to travel",
        "description" : `
        We created a mobile app that worked in a similar way as Tinder. You and your frineds liked or disliked locations on your phone.
        When you decided, our app recommended the location that best fit you or your whole group.
        You were able to make your connected groups of frineds.
        Most of the magic happened on backend where we implemented some form of machine learning for recommendations.
        Server side was written using Java and Spring framework. Built with the help of JHipster
        `
      },
      "skills" : ["Android", "Linux", "Java", "REST", "Spring", "Angular", "OpenAPI", "JHipster"]
    },
    {
      "headline" : "JunctionX 2019 Budapest",
      "description" : `
      Junction is a non-profit hackathon and tech community. Volunteer-led teams organize hackathons, speaker events, coding workshops and other tech events - around the year and around the world.

Th  event-filled year peaks annually with flagship event Junction: Europe's leading hackathon, yearly gathering around 1500 hackers together for a weekend-long experience. A meeting place for developers, designers and other techies, teaming up and creating new tech projects in 48 hours. This year, we are taking the event on a whole new level with Junction 2020 Connected, a hackathon like no other, gathering people all over the world to simultaneously hack in both physical locations and online.
      `,
      "tags" : ["hackathon"],
      "start" : "2019-11-15",
      "end" : "2019-11-17",
      "links" : ["https://www.hackjunction.com/concepts/junction-x", "https://github.com/trollrar/wizardApp", "https://github.com/aljazerzen/junctionxbudapest"],
      "media" : [{ "full" : "junctionx/photo1.jpg", "thumb" : "junctionx/photo1_thumb.jpg"}, { "full" : "junctionx/photo2.jpg", "thumb" : "junctionx/photo2_thumb.jpg"}, { "full" : "junctionx/photo3.jpg", "thumb" : "junctionx/photo3_thumb.jpg"}, { "full" : "junctionx/photo4.jpg", "thumb" : "junctionx/photo4_thumb.jpg"}, { "full" : "junctionx/photo5.jpg", "thumb" : "junctionx/photo5_thumb.jpg"}, { "full" : "junctionx/photo6.jpg", "thumb" : "junctionx/photo6_thumb.jpg"}, { "full" : "junctionx/photo7.jpg", "thumb" : "junctionx/photo7_thumb.jpg"}, { "full" : "junctionx/photo8.jpg", "thumb" : "junctionx/photo8_thumb.jpg"}, { "full" : "junctionx/photo9.jpg", "thumb" : "junctionx/photo9_thumb.jpg"}, { "full" : "junctionx/photo10.jpg", "thumb" : "junctionx/photo10_thumb.jpg"}, { "full" : "junctionx/photo11.jpg", "thumb" : "junctionx/photo11_thumb.jpg"}, { "full" : "junctionx/photo12.jpg", "thumb" : "junctionx/photo12_thumb.jpg"}, { "full" : "junctionx/photo13.jpg", "thumb" : "junctionx/photo13_thumb.jpg"}, { "full" : "junctionx/photo14.jpg", "thumb" : "junctionx/photo14_thumb.jpg"}, { "full" : "junctionx/photo15.png", "thumb" : "junctionx/photo15_thumb.png"}, { "full" : "junctionx/photo16.png", "thumb" : "junctionx/photo16_thumb.png"}, { "full" : "junctionx/video1.mp4", "thumb" : "junctionx/video1_thumb.png"}],
      "team" : {
        "name" : "Full in",
        "members" : ["Jakob Hostnik", "Miha Lazar", "Fares Villca", "Aljaž Eržen", "Mihael Verček"]
      },
      "place" : "Budapest",
      "project" : {
        "name" : "Augmented Reality Wizard application",
        "description" : `
          In a microsoft project we implemented a VR application. We created a hardware magic wand that you connected to your phone which you put into a VR headset. Then you could play magic battles against your friends or magical beasts. 
          In our challenge we had to use as much Azure cloud solutions as possible. So we used their machine learning to learn wand movements, speach recognition engine for casting spells and their AR engine and servers.
          To see how cool this project was it is best to check photos and a video.
        `
      },
      "skills" : ["VR", "AR", "ML", "speach recognition", "Android", "QR", "Raspberry PI", "Linux", "Java", "REST", "Spring", "Angular", "OpenAPI", "Arduino"]
    },
    {
      "headline" : "Signed contract with HPE d.o.o.",
      "tags" : ["work experience"],
      "start" : "2017-06-19",
      "end" : new Date(),
      "companyName" : "HPE d.o.o.",
      "place" : "Dolenjska cesta 83, Ljubljana",
      "links" : ["http://calms.com", "http://www.hpe.si/"],
      "description" : `
      HPE is a company dedicated to compressed air, to provide industrial compressors, and maintenance for those compressors.
      They have also software department named CALMS developing platform calms.com, expert system that allows energetic management and optimisation in industry.
      The system itself is a tool to provide a wholesome experience.
      Using IOT device "CALMS edge gateway" it reads data from multiple sources and sends data to calms.com.
      Data are processed on server and our system predicts potencial energy and money losses, generates reports, etc.
      Using an ultrasound sensor, device that you connect to your phone, you search for leaks of compressed air. Our system predicts losses and suggests actions that should be taken to save energy, money and emissions.
      CALMS is also a green platform and helps companies save trees by optimising their emissions.
      `,
      "companySize" : "5 developers",
      "media" : [{ "full" : "calms/icon", "thumb" : "calms/icon"}, { "full" : "calms/image1.png", "thumb" : "calms/image1_thumb.png"}, { "full" : "calms/image2.jpg", "thumb" : "calms/image2_thumb.jpg"}, { "full" : "calms/image3.png", "thumb" : "calms/image3_thumb.png"}, { "full" : "calms/image4.png", "thumb" : "calms/image4_thumb.png"}, { "full" : "calms/image5.png", "thumb" : "calms/image5_thumb.png"}, { "full" : "calms/image6.png", "thumb" : "calms/image6_thumb.png"}, { "full" : "calms/image7.png", "thumb" : "calms/image7_thumb.png"}],
      "linkedCards" : ["work experience", "role", "company:HPE"]
    },
    {
      "headline" : "Signed contract with DSE Group d.o.o.",
      "tags" : ["work experience"],
      "start" : "2016-10-01",
      "end" : "2016-10-31",
      "companyName" : "DSE Group d.o.o.",
      "place" : "Remote",
      "links" : ["http://tomasdse.com", "https://www.ajpes.si/podjetje/DSE_skupina,_d.o.o."],
      "description" : `
      I tried working part time during high school so I got employed by DSE Group.
      I developed TOMAS project in PHP and Yii framework. I had to quit the job after one month since high school required too much time.
      TOMAS project helps automate and organise internal company processes.
      `,
      "companySize" : "5 developers",
      "media" : [{ "full" : "tomasdse/image1.png", "thumb" : "tomasdse/image1_thumb.png"}, { "full" : "tomasdse/image2.png", "thumb" : "tomasdse/image2_thumb.png"}, { "full" : "tomasdse/image3.png", "thumb" : "tomasdse/image3_thumb.png"}, { "full" : "tomasdse/image4.png", "thumb" : "tomasdse/image4_thumb.png"}, { "full" : "tomasdse/image5.png", "thumb" : "tomasdse/image5_thumb.png"}, { "full" : "tomasdse/image6.png", "thumb" : "tomasdse/image6_thumb.png"}, { "full" : "tomasdse/image7.png", "thumb" : "tomasdse/image7_thumb.png"}, { "full" : "tomasdse/image8.jpg", "thumb" : "tomasdse/image8_thumb.jpg"}],
      "linkedCards" : ["work experience", "role", "company:DSE"],
    },
    {
      "headline" : "Full stack software developer",
      "description" : `
      Since the job lasted only one month it was mostly introduction. But I was quickly independent so I finished a few tasks before I letf.
      I learned how to work with Yii framework in PHP, its localisation and Postgres database.
      I got familiar with remote work in a company with no physical office.
      `,
      "tags" : ["work experience", "role", "company:DSE"],
      "start" : "2016-10-1",
      "end" : "2016-10-31",
      "links" : [],
      "media" : [],
      "projects" : {
        "TOMAS" : {"skills" : ["PhP (yii framework)", "Postgres", "Linux", "HTML", "jQuery", "CSS"]}
      }
    },
    {
      "headline": "Signed contract with Agitavit solutios d.o.o.",
      "tags" : ["work experience"],
      "start" : "2015-07-01",
      "end" : "2016-08-31",
      "companyName" : "Agitavit solutions d.o.o.",
      "place" : "Letališka cesta 33f, Ljubljana",
      "links" : ["https://www.agitavit.com"],
      "description" : `
       Agitavit is a company dedicated completely to software development.
       When I finished the second year in my technical grammar school I tried to get a job and contribute to society.
       I applied for a few jobs, not because I had to but because I wanted to.
       So as a 16 year-old I was accepted as a Junior developer.
       It was my first job in the software development field.
       On my proposal we had a deal that while they are teaching me I do not get paid. 
       But we agreed when I start contributing to the company I get a fair payment.
       I recieved a proper mentourship there and was expanding my knowledge rapidly.
      `,
      "companySize" : "50 developers",
      "media" : [{ "full" : "agitavit/image1.jpg", "thumb" : "agitavit/image1_thumb.jpg"}, { "full" : "agitavit/image2.jpg", "thumb" : "agitavit/image2_thumb.jpg"}, { "full" : "agitavit/image3.jpg", "thumb" : "agitavit/image3_thumb.jpg"}, { "full" : "agitavit/image4.jpg", "thumb" : "agitavit/image4_thumb.jpg"}],
      "linkedCards" : ["work experience", "role", "company:Agitavit"]
    },
    {
      "headline" : "Junior software developer",
      "description" : `
      In the first few weeks I was learning about new technologies and got familiar with the process in this company.
      Then we agreed that I am ready to be useful in the company and I got my first tasks on the biggest project we had.
      I learned how to work in a team, how to use version control (team fundation server) and how to develop web applications.
      I properly learned SQL, work with database, CRUD, and other technologies and principles.
      For the last month they assigned me my own project for internal use where I used MVC framework.
      `,
      "tags" : ["work experience", "role", "company:Agitavit"],
      "start" : "2015-07-01",
      "end" : "2015-08-31",
      "links" : [],
      "media" : [],
      "projects" : {
        "Web application for a big farmacy partner" : {"skills" : ["TFS", "C#", "ASP.Net", "WebForms", "HTML", "CSS", "JavaScript", "jQuery", "MSSQL"]},
        "Web application for internal use" : {"skills" : ["TFS", "C#", "ASP.Net", "HTML", "CSS", "JavaScript", "jQuery", "MSSQL", "MVC5+", "Entity Framework 6"]}
      }
    },
    {
      "headline" : "Software developer",
      "description" : `
      In the Summer after my third year of high school I worked in Agitavit again. I mostly worked on a pharmacy project, added new features and fixed bugs that showed in a year on the web application for internal use.
      `,
      "tags" : ["work experience", "role", "company:Agitavit"],
      "start" : "2016-06-21",
      "end" : "2016-08-31",
      "links" : [],
      "media" : [],
      "projects" : {
        "Web application for a big farmacy partner" : {"skills" : ["C#", "ASP.Net", "WebForms", "HTML", "CSS", "JavaScript", "jQuery", "MSSQL"]},
        "Web application for internal use" : {"skills" : ["C#", "ASP.Net", "HTML", "CSS", "JavaScript", "jQuery", "MSSQL", "MVC5+", "Entity Framework 6"]}
      }
    },
    
        // hacking
    {
      "headline" : "Hacked US government institution",
      "description" : "Since it is a grey zone topic please contact me in person for more information.",
      "skills" : ["hacking", "vulnerability discovery", "SQL", "Databases", "Networking"],
      "tags" : ["hacking"],
      "start" : "2018",
    },
    {
      "headline" : "Hacked a small website",
      "description" : "Since it is a grey zone topic please contact me in person for more information.",
      "vulnerability" : "SQL injection",
      "skills" : ["hacking", "vulnerability discovery", "SQL", "Databases", "Networking"],
      "tags" : ["hacking"],
      "start" : "2017",
    },
    {
      "headline" : "Hacked a public hardware of payed service",
      "description" : "Since it is a grey zone topic please contact me in person for more information.",
      "vulnerability" : "Privilege escalation",
      "tags" : ["hacking"],
      "skills" : ["hacking", "vulnerabily discovery", "Linux", "networking"],
      "start" : "2015",
    },
    {
      "headline" : "Found security vulnerability in Edge and Chrome browser",
      "description" : "Since it is a grey zone topic please contact me in person for more information.",
      "vulnerability" : "Privilege escalation",
      "tags" : ["hacking"],
      "skills" : ["hacking", "vulnerabily discovery", "JavaScript", "HTML", "CSS", "web browsers"],
      "start" : "2018",
    },
    {
      "headline" : "My school mate and I created a few infected USB sticks.",
      "description" : `
      If you found this usb stick there was only a gif image.
      By the time you watched the whole gif we had access to your whole computer.
      Since it is a grey zone topic please contact me in person for more information.`,
      "vulnerability" : "Privilege escalation",
      "tags" : ["hacking"],
      "skills" : ["hacking", "Windows", "Linux"],
      "start" : "2017",
    },
    {
      "headline" : "First programming experiences",
      "links" : ["https://zmaga.com", "https://zmaga.com/content.php?id=1848"],
      "description" : `
      When I was in 12 years old in the 6th grade of primary school my older brother showed me how to create a simple program in visual basic.
      Together we created a few calculators and a program that calculated prime numbers using a few loops and if conditions.
      Together meaning my brother did all the work and I was watching but understood nothing.
      He learned how to program in Faculty of Civil Engineering.

      That same year I found out about a website called zmaga.com which was full of different tutorials on all kinds of topics.
      There was a whole section about computers and programming.
      I learned how to create my first virus there - a simple bat file that called itself until you had to restart your computer.
      And I also learned Visual Basic, about variables, loops, etc.
      My first programs were calculators for areas and volumes of objects.
      Then I created a program where you could draw a pyramid and it would dynamically calculate its volume and area.

      I created my first game in visual basic following a tutorial. 
      It was a simple game where you popped baloons by clicking.
      The baloons were appearing in random spots and you had one minute to pop as much baloons as possible.
      I also added an animation of popping and high score.
      I wanted the game to save the high score even if you closed it.
      So I started to follow tutorials on vbtutor.net so I would learn how to do that.

      A section about saving data was about SQL databases that I did not understood.
      When I dicsussed this topic with my brother he told me it was beyond his knowlede and brought me a book about Java and told me to learn a more serious language.
      So I started learning Java.
      I was learning about objects and writing console and graphical applications, I also learned what databases are and how to work with them.
      And I enjoyed it.

      Meanwhile my other brother taught me how to work in Adobe Photoshop and I also followed a lot of tutorials on zmaga.com.
      `,
      "tags" : ["begginings"],
      "skills" : ["Visual Basic", "Java", "SQL"],
      "start" : "2009",
      "end" : "2013"
    },
    {
      "headline" : "Cofound a startup - Etilk d.o.o.",
      "links" : ["https://musips.com", "https://www.ajpes.si/podjetje/ETILK_d.o.o.?enota=697391&EnotaStatus=1"],
      "description" : `
      When me and my wife were 19 years old we founded our own company - Etilk d.o.o.
      Since the beginning we started a few projects and tried to find our niche.
      We worked on an automatic GPS tracking system for football teams, summer singing camps, a project that draws your song, and the project Musips - a search engine and social networking platform for musicians.
      Most of the projects were not finished because of limited time and resources in college. 
      Now the company is focused on choirs for everyone which works very well since my wife is a choral conductor. 
      I work on accounting, advertisment, customer satisfiction and the website.
      `,
      "tags" : ["entrepreneurship"],
      "skills" : ["enterpreneurship", "online advertisement", "sales", "customer satisfiction", "accounting", "3D printing", "CNC milling", "circuit design"],
      "start" : "2018",
      "end" : new Date()
    },
    {
      "headline" : "Catholic Scouts",
      "links" : ["https://skavti.si/"],
      "description" : `I have been a scout since the second grade of primary school and in the mean time I have learned a lot of skills.
      There was a lot of volunteering, leadership and teamwork.
      `,
      "tags" : ["hobbies", "volunteering"],
      "skills" : ["leadership", "survival", "team work", "positive attitude to nature"],
      "start" : "2005",
      "end" : new Date()
    },
    {
      "headline" : "Sports and school activities",
      "links" : [],
      "description" : `In primary school I trained handball and football for a few years and I took art classes.`,
      "tags" : ["hobbies"],
      "skills" : ["healthy life style", "socialization", "art", "football", "handball"],
      "start" : "2004",
      "end" : "2013"
    },
    {
      "headline" : "Extra curricular activity",
      "links" : [],
      "description" : `
      In primary school I trained handball and football for a few years and I also took art and drama classes.
      I attended a lot of school competitions. 
      I got three silver diplomas in mathematics and a first place in physics in school competitions.
      `,
      "tags" : ["hobbies"],
      "skills" : ["healthy life style", "socialization", "art", "football", "handball"],
      "start" : "2004",
      "end" : "2013"
    },
    {
      "headline" : "Volunteering in a retirement home",
      "links" : [],
      "description" : `In primary school I also volunteered in a retirenment home.
      I was there to keep company to lonely people.`,
      "tags" : ["volunteering"],
      "skills" : ["team work", "talk", "soft skills"],
      "start" : "2004",
      "end" : "2013"
    },
    {
      "headline" : "Wedding",
      "links" : [],
      "description" : `When I was 22 years old I married my wife Tina Hostnik.`,
      "tags" : ["life events"],
      "start" : "2020-10-03",
    },
    {
      "headline" : "Birth",
      "links" : [],
      "description" : `
      I was born as an 8th child into a big family. 
      Later I got a younger brother and sister.
      `,
      "tags" : ["life events"],
      "start" : "1998-06-20"
    }
  ]
  
};

// add blog
// add certifications
//  "certifications" : {
//    "name" : "",
//    "start" : "",
//    "description" : "",
//    "organisation" : "",
//    "links" : []
//  },
