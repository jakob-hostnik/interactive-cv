function filterCards() {
  console.log("filter tags");
  var activeTags = Array.from(document.querySelectorAll("#filter-tags button.tag.active")).map(e=>e.innerText);
  var activeSkills = Array.from(document.querySelectorAll("#filter-skills button.tag.active")).map(e=>e.innerText);
  document.querySelectorAll(".timeline .card").forEach(card=>{
    if (activeTags.length == 0 && activeSkills == 0) {
  		card.classList.remove("d-none");
    } else {
  	  var anySelected = false;
  	  activeTags.forEach(tag=>{
  	  	if (card.getAttribute("data-tags").split(";").includes(tag)) {
  	  		anySelected = true;
  	  	}
  	  });
  	  activeSkills.forEach(skill=>{
  	  	if (card.getAttribute("data-skills").split(";").includes(skill)) {
  	  		anySelected = true;
  	  	}
        card.querySelectorAll("*[data-skills]").forEach(cardElement=>{
          if (cardElement.getAttribute("data-skills").split(";").includes(skill)) {
  	  		  anySelected = true;
          }
        });
  	  });
      if (anySelected) {
  	  	card.classList.remove("d-none");
  	  } else {
  	  	card.classList.add("d-none");
  	  }
    }
  });
}

function loadFilters() {
  var skills = [];
  document.querySelectorAll(".timeline *[data-skills]").forEach(e => {skills = skills.concat(e.getAttribute('data-skills').split(';'))});
  skills = [...new Set(skills)];
  skills.splice(skills.indexOf(""), 1);

  var tags = [];
  document.querySelectorAll(".timeline *[data-tags]").forEach(e => {tags = tags.concat(e.getAttribute('data-tags').split(';'))});
  tags = [...new Set(tags)];
  //tags.splice(tags.indexOf(""), 1);
  skills.sort().forEach(skill => {
    let htmlElement=`<button class="filter-tag-btn btn btn-outline-secondary tag" data-skill="${skill}">${skill}</button>`;
    document.querySelector("#filter-skills").insertAdjacentHTML('beforeend', htmlElement);
  });

  tags.sort().forEach(tag => {
          let htmlElement=`<button class="filter-tag-btn btn btn-outline-primary tag" data-tag="${tag}">${tag}</button>`;
    document.querySelector("#filter-tags").insertAdjacentHTML('beforeend', htmlElement);
  });
}

function registerLightBox() {
}

function registerEvents() {
  document.querySelectorAll("#filter-tags button.tag, #filter-skills button.tag").forEach(element => {
    element.addEventListener("click", function(e) {
      if(e.target.classList.contains("active")) {
        e.target.classList.remove("active");
      } else {
        e.target.classList.add("active");
      }
      filterCards();
    }); 
  });
}

function activateTag(tag) {
  console.log(tag);
  document.querySelectorAll("#filter-skills button.tag, #filter-tags button.tag").forEach(b=>{
    b.classList.remove("active");
  });
  document.querySelector(`#filter-tags button.tag[data-tag='${tag}']`).classList.add("active");
  filterCards();
  document.querySelector(`#filter-tags button.tag[data-tag='${tag}']`).scrollIntoView();
  document.querySelector(".filter-card").scrollIntoView();
}
function activateSkill(skill) {
  document.querySelectorAll("#filter-skills button.tag, #filter-tags button.tag").forEach(b=>{
    b.classList.remove("active");
  });
  document.querySelector(`#filter-skills button.tag[data-skill='${skill}']`).classList.add("active");
  filterCards();
  document.querySelector(`#filter-skills button.tag[data-skill='${skill}']`).scrollIntoView();
  document.querySelector(".filter-card").scrollIntoView();
}
